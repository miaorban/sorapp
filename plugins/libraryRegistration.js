import __Beers from '~/library/beersApiCalls.js'
import __Breweries from '~/library/breweriesApiCalls.js'

export default (ctx, inject) => {
  const Beers = new __Beers(ctx)
  ctx.Beers = Beers
  inject('Beers', Beers)

  const Breweries = new __Breweries(ctx)
  ctx.Breweries = Breweries
  inject('Breweries', Breweries)
}
