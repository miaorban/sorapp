import Vue from 'vue'
// import bModal from 'bootstrap-vue/es/components/modal/modal'
// import bModalDirective from 'bootstrap-vue/es/directives/modal/modal'
// import { userInfo } from '../mixins/global.js'
import moment from 'moment'

// Vue.mixin(userInfo)
// Vue.component('b-modal', bModal)
// Vue.directive('b-modal', bModalDirective)

Vue.filter('formatDate', function (datum) {
  if (!datum) return ''
  return moment(datum).format('YYYY-MM-DD')
})
// global read only variable to get if the user is admin or not
// Vue.mixin({
//   data: function() {
//     return {
//       get globalReadOnlyIsAdmin() {
//         console.log('ssss ', this.store.state.jog);
//         return true
//       }
//     }
//   }
// })
