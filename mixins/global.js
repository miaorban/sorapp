// Minden komponenshez szukseges felhasznaloi informaciok kezelese

export const userInfo = {
  data: function () {
    return {
      isadmin: false
    }
  },
  created: function () {
    this.$_userInfo_jogosultsagBeallitasa()
  },
  methods: {
    // beallitjuk, hogy a felhasznalo admin-e
    $_userInfo_jogosultsagBeallitasa () {
      this.$store.state.jog === 1 ? this.isadmin = true : this.isadmin = false
    }
  }
}
