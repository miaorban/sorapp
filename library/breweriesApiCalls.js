import axios from 'axios'
// import _ from 'lodash'

export default class Brewery {
  constructor (ctx) {
    this.$axios = ctx.$axios
  }
  // sorfozde mentese vagy frissitese
  async upsertOne (queryObject) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3002/brewery/upsert', queryObject)
        .then((brewery) => {
          resolve(brewery.data)
        })
        .catch(error => reject(error))
    })
  }

  // osszes sor
  async getAll (queryObject) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/brewery/collect')
        .then((breweries) => {
          resolve(breweries.data)
        })
        .catch(error => reject(error))
    })
  }

  // osszes sorfozde nevenek lekerese
  async getAllNames (queryObject) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/brewery/collectnames')
        .then((breweries) => {
          resolve(breweries.data)
        })
        .catch(error => reject(error))
    })
  }

  async getByName (name) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/brewery/getbyname/' + name)
        .then((beerdetails) => {
          resolve(beerdetails.data[0])
        })
        .catch(error => reject(error))
    })
  }

  async topUpdated (name) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/brewery//lastUpdated/5')
        .then((breweries) => {
          resolve(breweries.data)
        })
        .catch(error => reject(error))
    })
  }
}
