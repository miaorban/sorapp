import axios from 'axios'

export default class Beer {
  constructor (ctx) {
    this.$axios = ctx.$axios
  }
  // sor mentese vagy frissitese
  async upsertOne (queryObject) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3002/beer/upsert', queryObject)
        .then((beer) => {
          resolve(beer.data)
        })
        .catch(error => reject(error))
    })
  }

  // osszes sor
  async getAll (queryObject) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/beer/collect')
        .then((beers) => {
          resolve(beers.data)
        })
        .catch(error => reject(error))
    })
  }

  // lehetséges kategóriák lekérése
  async getAllCategories (queryObject) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/beer/collect/categories')
        .then((categories) => {
          resolve(categories.data)
        })
        .catch(error => reject(error))
    })
  }

  // sör törlése név és sörfőzde alapján
  async deleteOne ({ id }) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3002/beer/delete', { beer_id: id })
        .then((response) => {
          resolve(response.data)
        })
        .catch(error => reject(error))
    })
  }

  // sör törlése név és sörfőzde alapján
  async getByName (name, breweryid) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/beer/getbyname/' + name + '/' + breweryid)
        .then((beerdetails) => {
          resolve(beerdetails.data[0])
        })
        .catch(error => reject(error))
    })
  }

  // sör törlése név és sörfőzde alapján
  async topAbv () {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3002/beer/abv/5')
        .then((beers) => {
          resolve(beers.data)
        })
        .catch(error => reject(error))
    })
  }
}
